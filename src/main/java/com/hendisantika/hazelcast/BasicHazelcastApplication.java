package com.hendisantika.hazelcast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasicHazelcastApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasicHazelcastApplication.class, args);
    }
}
